<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>
    <link rel="stylesheet" href="site/css/contact.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>

<body>
    <div class="header">
        <div class="section-1">
            <div class="section-1">
                <div class="container">
                    <nav class="navbar navbar-expand-lg ">
                        <div class="container-fluid py-4 my-3 px-0 mx-0">
                            <a class="navbar-brand" href="#">
                                <img src="site/image/logo.png" alt="" srcset="">
                            </a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse offset-md-1" id="navbarNav">
                                <div class="mr-auto"></div>
                                <ul class="navbar-nav ms-md-4">
                                    <li class="nav-item">
                                        <a class="nav-link" aria-current="page" href="/">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="about">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="product">Products</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="contact">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="title  pb-md-3 ms-md-5">
                <h1>Contact Us</h1>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-panel-contact1 ms-md-5">
                        <h1 class="p-0 m-0">Stephen DeGruchy</h1>
                        <i class="p-0 mb-0">Owner/Commodity Trader</i><br>
                        <br>
                        <p class="p-0 m-0">Toll Free – 877-496-0047</p>
                        <p class="p-0 m-0">Direct – 587-329-2216</p>
                        <p class="p-0 m-0">Cell – 403-880-2091</p>
                        <p class="p-0 m-0">Email – stephen.degruchy@caliperfp.com</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="image-contact">
                        <img src="site/image/contact-1.jpg" alt="" srcset="" width="90%">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="image-contact">
                        <img src="site/image/contact-2.jpg" alt="" srcset="" width="90%">
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="text-panel-contact2">
                        <h1 class="p-0 m-0">Stacy Excell</h1>
                        <i class="p-0 m-0">Administrative Lead</i>
                        <br>
                        <p class="p-0 m-0">Toll Free – 877-496-0047</p>
                        <p class="p-0 m-0">Direct – 587-329-2216</p>
                        <p class="p-0 m-0">Cell – 403-880-2091</p>
                        <p class="p-0 m-0">Email – accounting@caliperfp.com</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="text-panel-contact3 ms-md-5">
                        <h1 class="p-0 m-0">Dean Rosell</h1>
                        <i class="p-0 m-0">Yard Manager (Black Diamond, Alberta)</i>
                        <br>
                        <p class="p-0 m-0">Cell – 403-880-2091</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="image-contact pb-5">
                        <img src="site/image/contact-3.jpg" alt="" srcset="" width="90%">
                    </div>
                </div>
            </div>
            <div class="section-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <div class="touch">
                                <p>Let's keep in touch</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Your Name</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1">

                                <label for="exampleFormControlInput1">Your Email(required)</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1">

                                <label for="exampleFormControlInput1">Subject</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1">

                                <label for="exampleFormControlInput1">Your Message</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <button type="submit" class="btn btn-primary mt-5">Send</button>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-6">
                        <div class="text1">
                            <h1 class="p-0 m-0">Head office:</h1>
                            <p class="p-0 m-0">Suite 340, 715 – 5th Ave S.W <br>
                                Calgary, Alberta <br>
                                T2P 2X6</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text2">
                            <h1 class="p-0 m-0">Lumber Yard:</h1>
                            <p class="p-0 m-0">394061 168th Street W <br>
                                Black Diamond, Alberta <br>
                                T0L 0H0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>